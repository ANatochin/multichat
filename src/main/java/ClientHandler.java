import java.io.*;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class ClientHandler implements Runnable{

    private static List<ClientHandler> handlers = new ArrayList<>();
    private Socket socket;
    private BufferedReader reader;
    private BufferedWriter writer;
    private String clientName;

    public ClientHandler(Socket socket){
        try{
            this.socket = socket;
            this.writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(), StandardCharsets.UTF_8));
            this.reader = new BufferedReader(new InputStreamReader(socket.getInputStream(),StandardCharsets.UTF_8));
            this.clientName = reader.readLine();

            if (handlers.size() >=3) {
                writer.write("chat is full");
                writer.newLine();
                writer.flush();

                closer(socket, reader, writer);
            }

            handlers.add(this);

            distributeMessage(this.clientName + " entered chat");
        } catch (IOException e){
            closer(socket, reader, writer);
        }
    }

    @Override
    public void run() {
        String msgFmClient;

        while(true){ //socket.isConnected()
            try{
                msgFmClient = this.reader.readLine();

                if(msgFmClient.equalsIgnoreCase("exit")){
                    closer(socket, reader, writer);
                    removeClient();
                    break;
                }

                distributeMessage(msgFmClient);
            }catch (IOException e){
                closer(socket, reader, writer);
                break;
            }
        }
    }

    public void distributeMessage(String msg){
        for(ClientHandler handler : handlers){
            try{
                if (!handler.clientName.equals(clientName)) {
                    handler.writer.write(msg);
                    handler.writer.newLine();
                    handler.writer.flush();
                }
            } catch (IOException e){
                closer(socket, reader, writer);
            }
        }
    }

    public void removeClient(){
        handlers.remove(this);
        distributeMessage(clientName + " left");
    }

    public void closer(Socket socket, BufferedReader reader, BufferedWriter writer){
        removeClient();
        try{
            if(reader!=null){
                reader.close();
            }
            if(writer!=null){
                writer.close();
            }
            if(socket!=null){
                socket.close();
            }
        } catch (IOException e){
            e.printStackTrace();
        }
    }
}
