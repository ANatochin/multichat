import java.io.*;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class Client {
    private Socket socket;
    private BufferedReader reader;
    private BufferedWriter writer;
    private String name;

    public Client(Socket socket, String name){
        try{
            this.socket = socket;
            this.writer = new BufferedWriter(new OutputStreamWriter(this.socket.getOutputStream(), StandardCharsets.UTF_8));
            this.reader = new BufferedReader(new InputStreamReader(this.socket.getInputStream(), StandardCharsets.UTF_8));
            this.name = name;
        } catch (IOException e){
            closer(socket, reader, writer);
        }
    }

    public void sendMessage(){
        try{
            writer.write(this.name);
            writer.newLine();
            writer.flush();

            Scanner scanner = new Scanner(System.in);
            while(true){ //socket.isConnected()
                String message = scanner.nextLine();

                if(message.equalsIgnoreCase("exit")){
                    closer(socket,reader,writer);
                    break;
                }

                writer.write(this.name + " " + message);
                writer.newLine();
                writer.flush();
            }
        } catch (IOException e){
            closer(socket, reader, writer);
        }
    }

    public void messageReceiver(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                String messageFmChat;

                while(socket.isConnected()){
                    try{
                        messageFmChat = reader.readLine();
                        System.out.println(messageFmChat);
                    }catch(IOException e){
                        closer(socket, reader, writer);
                    }
                }
            }
        }).start();
    }

    public void closer(Socket socket, BufferedReader reader, BufferedWriter writer){

        try{
            if(reader!=null){
                reader.close();
            }
            if(writer!=null){
                writer.close();
            }
            if(socket!=null){
                socket.close();
            }
        } catch (IOException e){
            e.printStackTrace();
        }

    }

    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter name ");
        String name =  scanner.nextLine();

        Socket socket = new Socket("localhost", 8080);

        Client client = new Client(socket, name);
        client.messageReceiver();
        client.sendMessage();
    }

}
